package com.connecty.connecty;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static android.net.wifi.WifiConfiguration.Status.CURRENT;
import static android.net.wifi.WifiConfiguration.Status.ENABLED;

public class ScanAndConnectRunnable implements Runnable {

    WifiManager wifiManager;
    LocalBroadcastManager localBroadcastManager;
    WifiReceiver receiverWifi;
    List<ScanResult> wifiList;
    StringBuilder sb = new StringBuilder();


    public ScanAndConnectRunnable(WifiManager wifiManager, LocalBroadcastManager localBroadcastManager) {
        this.wifiManager = wifiManager;
        this.localBroadcastManager = localBroadcastManager;
    }

    @Override
    public void run() {
        Log.d("Connecty", " Scan And Connect");
        scanAndConnect();
    }

    private void scanAndConnect() {
        if (!wifiManager.isWifiEnabled()) {
            // If wifi disabled then enable it
            //Toast.makeText(getApplicationContext(), "wifi is disabled..making it enabled",Toast.LENGTH_LONG).show();

            wifiManager.setWifiEnabled(true);
        }

        Log.d("Connecty", " Scan And Connect");
        receiverWifi = new WifiReceiver();
        //registerReceiver(receiverWifi, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));

        wifiManager.startScan();
        List<ScanResult> scanResults = wifiManager.getScanResults();
        //System.out.println(scanResults);

        List<Network> nearNetworks = getAvailableNetworks(scanResults);
        //Log.d("Connecty", "find near Networks" + nearNetworks);
        final Network strongestNetwork = getStrongestNetwork(nearNetworks);
        connectIfNotConnected(strongestNetwork);

    }

    private String restGet(String urlString) throws Exception {
        StringBuilder chaine = new StringBuilder("");
        URL url = new URL(urlString);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestProperty("User-Agent", "");
        connection.setRequestMethod("GET");
        connection.setDoInput(true);
        connection.connect();

        InputStream inputStream = connection.getInputStream();

        BufferedReader rd = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        while ((line = rd.readLine()) != null) {
            chaine.append(line);
        }
        return chaine.toString();
    }


    private void connectIfNotConnected(Network strongestNetwork) {
        if (strongestNetwork != null) {
            Log.d("Connecty", wifiManager.getConnectionInfo().getSSID().replaceAll("^\"(.*)\"$", "$1"));
            Log.d("Connecty", strongestNetwork.getSsid());

            if (!wifiManager.getConnectionInfo().getSSID().replaceAll("^\"(.*)\"$", "$1").equals(strongestNetwork.getSsid())) {
                connectToNetwork(strongestNetwork);
            }else{
                Intent intent = new Intent("change-network-event");
                intent.putExtra("message", strongestNetwork.getSsid());
                localBroadcastManager.sendBroadcast(intent);
            }
        }
    }

    private void connectToNetwork(Network strongestNetwork) {
        WifiConfiguration wifiConfig = new WifiConfiguration();
        wifiConfig.SSID = String.format("\"%s\"", strongestNetwork.getSsid());
        if (strongestNetwork.getPassword()!= null){
            wifiConfig.preSharedKey = String.format("\"%s\"", strongestNetwork.getPassword());
        }
        Log.d("Connecty", "connecting " + strongestNetwork.getSsid());

        wifiConfig.networkId = getExistingNetwork(strongestNetwork.getSsid());

        if (wifiConfig.networkId == -1) {
            wifiConfig.networkId = wifiManager.addNetwork(wifiConfig);
        }
        boolean reconnect = false;
        if (wifiConfig.networkId != -1) {
            wifiManager.disconnect();
            wifiManager.enableNetwork(wifiConfig.networkId, true);
            reconnect = wifiManager.reconnect();
        }

        Log.d("Connecty", "Broadcasting message, connection result " + reconnect);
        Intent intent = new Intent("change-network-event");

        String connectingMessage;

        if (strongestNetwork != null && reconnect) {
            connectingMessage = "Connected to " + strongestNetwork.getSsid();
        } else {
            connectingMessage = "no network";
        }

        intent.putExtra("message", connectingMessage);
        localBroadcastManager.sendBroadcast(intent);
    }



    private Integer getExistingNetwork(String myNetworksSSID) {
        Integer networkId = -1;
        for (WifiConfiguration config : wifiManager.getConfiguredNetworks()) {
            String newSSID = config.SSID;

            if (myNetworksSSID.equals(newSSID.replaceAll("^\"(.*)\"$", "$1"))) {
                networkId = config.networkId;
                break;
            }
        }
        return networkId;
    }

    @NonNull
    private List<Network> getApplicationNetworks() {

        List<Network> networkList = new ArrayList<>();

        try {
            String jsonString = restGet("http://188.166.52.158/api/network");
            JSONArray jsonArray = null;

            jsonArray = new JSONArray(jsonString);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonNetwork = jsonArray.getJSONObject(i);
                String ssid = jsonNetwork.getString("ssid");
                String password = jsonNetwork.getString("password");
                Network network = new Network(ssid, password);
                networkList.add(network);
            }

        } catch (Exception e) {
            Network annalisaNetwork = new Network("Mr.iPhone", "coolpass");
            Network maksNetwork = new Network("Maksim's iPhone", "12345678M");
            Network marioNetwork = new Network("punked", "98765432");
            Network townside1 = new Network("Townside Hotspot 1", "viertel78015");
            Network townside2 = new Network("Townside Hotspot 2", "viertel78015");
            networkList.add(annalisaNetwork);
            networkList.add(maksNetwork);
            networkList.add(marioNetwork);

            networkList.add(townside1);
            networkList.add(townside2);
            System.out.println("Big failure: " + e);
            e.printStackTrace();
        }
        return networkList;
    }

    @Nullable
    private Network getStrongestNetwork(List<Network> nearNetworks) {
        if (nearNetworks != null && nearNetworks.size() > 0) {
            Network strongestNetwork = null;

            for (Network network1 : nearNetworks) {
                for (Network network2 : nearNetworks) {
                    if (network1.getSsid() != network2.getSsid() && WifiManager.compareSignalLevel(network1.getScanResult().level, network2.getScanResult().level) >= 0) {
                        strongestNetwork = network1;
                    }
                }
            }
            return strongestNetwork;
        }
        return null;
    }

    @NonNull
    private List<Network> getAvailableNetworks(List<ScanResult> scanResults) {

        List<Network> nearFreeNetworks = getFreeNetworks(scanResults);
        List<Network> nearConfiguredNetworks = getNearExistingSavedNetworks(scanResults);
        List<Network> nearDatabaseNetworks = getNearDatabaseNetworks(scanResults);

        List<Network> availableNetworks = new ArrayList<>();
        availableNetworks.addAll(nearFreeNetworks);
        availableNetworks.addAll(nearConfiguredNetworks);
        availableNetworks.addAll(nearDatabaseNetworks);

        return availableNetworks;
    }

    private List<Network> getNearExistingSavedNetworks(List<ScanResult> scanResults) {
        List<Network> existingSavedNetworkList = new ArrayList<>();

        for (WifiConfiguration wifiConfig : wifiManager.getConfiguredNetworks()) {
            Network network = new Network(wifiConfig.SSID.replaceAll("^\"(.*)\"$", "$1"), null);
            if (ENABLED == wifiConfig.status || CURRENT == wifiConfig.status) {
                existingSavedNetworkList.add(network);
            }
        }
        return getNearNetworks(scanResults, existingSavedNetworkList);
    }

    private List<Network> getNearDatabaseNetworks(List<ScanResult> scanResults) {
        List<Network> networkList = getApplicationNetworks();
        return getNearNetworks(scanResults, networkList);
    }

    private List<Network> getNearNetworks(List<ScanResult> scanResults, List<Network> networkList) {
        List<Network> nearDatabaseNetworks = new ArrayList<>();
        for (ScanResult scanResult : scanResults) {
            for (Network network : networkList) {
                if (scanResult.SSID.equals(network.getSsid())) {
                    network.setScanResult(scanResult);
                    nearDatabaseNetworks.add(network);
                }
            }
        }
        return nearDatabaseNetworks;
    }

    @NonNull
    private List<Network> getFreeNetworks(List<ScanResult> scanResults) {
        List<Network> freeNetworks = new ArrayList<>();

        for (ScanResult scanResult : scanResults) {
            if (isFree(scanResult)) {
                Network network = new Network(scanResult.SSID, null);
                network.setScanResult(scanResult);
                freeNetworks.add(network);
            }
        }
        return freeNetworks;
    }

    static Boolean isFree(ScanResult result) {
        if (result.capabilities.contains("WEP")) {
            return false;
        } else if (result.capabilities.contains("PSK")) {
            return false;
        } else if (result.capabilities.contains("EAP")) {
            return false;
        }
        return true;
    }

    class WifiReceiver extends BroadcastReceiver {

        // This method call when number of wifi connections changed
        public void onReceive(Context c, Intent intent) {

            sb = new StringBuilder();
            wifiList = wifiManager.getScanResults();
            sb.append("\n    Number Of Wifi connections :")
                    .append(wifiList.size())
                    .append("\n\n");

        }
    }


}
