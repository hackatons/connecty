package com.connecty.connecty;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.CompoundButton;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import static android.widget.CompoundButton.*;

public class OnOffWifiScannerChangeListener implements OnCheckedChangeListener {
    WifiManager wifiManager;
    ScanAndConnectRunnable scanAndConnect;
    UsageMonitoringRunnable usageMonitoringRunnable;

    ScheduledFuture<?> scheduledFutureScanAndConnect = null;
    ScheduledFuture<?> scheduledFutureUsageMonitoring= null;
    ScheduledExecutorService scheduledExecutorService;

    public OnOffWifiScannerChangeListener(Context context) {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(context);

        this.wifiManager = wifiManager;
        scanAndConnect = new ScanAndConnectRunnable(wifiManager, localBroadcastManager);
        usageMonitoringRunnable = new UsageMonitoringRunnable(localBroadcastManager);
        scheduledExecutorService = Executors.newScheduledThreadPool(4);

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        Log.d("Connecty", "changing state");
        if (isChecked) {
            Log.d("Connecty", "turning on");
            scheduledFutureScanAndConnect = scheduledExecutorService.scheduleWithFixedDelay(scanAndConnect, 0, 5, TimeUnit.SECONDS);
            scheduledFutureUsageMonitoring = scheduledExecutorService.scheduleWithFixedDelay(usageMonitoringRunnable, 0, 1, TimeUnit.SECONDS);
        } else if (scheduledFutureScanAndConnect != null && scheduledFutureUsageMonitoring != null ) {
            Log.d("Connecty", "turning off");
            scheduledFutureScanAndConnect.cancel(true);
            scheduledFutureUsageMonitoring.cancel(true);
        }
    }
}