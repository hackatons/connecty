package com.connecty.connecty;

import android.net.wifi.ScanResult;

/**
 * Created by eduaro on 29.08.15.
 */
public class Network {
    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private String ssid;
    private String password;
    private ScanResult scanResult;

    public Network(String ssid, String password) {
        this.ssid = ssid;
        this.password = password;
    }

    public ScanResult getScanResult() {
        return scanResult;
    }

    public void setScanResult(ScanResult scanResult) {
        this.scanResult = scanResult;
    }


}
