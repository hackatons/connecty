package com.connecty.connecty;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.TrafficStats;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Switch;
import android.widget.TextView;

import static android.text.format.Formatter.formatShortFileSize;


public class init extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_init);
        Switch onOffSwitch = (Switch) findViewById(R.id.onOffSwitch);

        //set the switch to ON
        onOffSwitch.setChecked(false);
        //attach a listener to check for changes in state
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);

        localBroadcastManager.registerReceiver(mMessageReceiver, new IntentFilter("change-network-event"));

        localBroadcastManager.registerReceiver(mUsageReceiver, new IntentFilter("change-usage-event"));

        onOffSwitch.setOnCheckedChangeListener(new OnOffWifiScannerChangeListener(this.getApplicationContext()));


        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        TextView networkSsid = (TextView) findViewById(R.id.networkSsid);
        networkSsid.setText(networkInfo.getExtraInfo());

        TextView usageText = (TextView) findViewById(R.id.usageText);
        String formatredUsageChange = formatShortFileSize(this.getApplicationContext(), Long.valueOf(TrafficStats.getTotalRxBytes()));

        usageText.setText(String.valueOf(formatredUsageChange));


        //ScheduledFuture sf = scheduledPool.schedule(callabledelayedTask, 4, TimeUnit.SECONDS);

        //String value = sf.get();

        //System.out.println("Callable returned"+value);

        //scheduledPool.shutdown();
        //scheduledPool.shutdown();

        //System.out.println("Is ScheduledThreadPool shutting down? " + scheduledPool.isShutdown());


    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("message");
            Log.d("connecty", "Got message: " + message);
            TextView networkSsid = (TextView) findViewById(R.id.networkSsid);
            networkSsid.setText(message);


        }
    };

    private BroadcastReceiver mUsageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String usageChange = intent.getStringExtra("usageChange");
            Log.d("connecty", "Got usage: " + usageChange);

            String formatredUsageChange = formatShortFileSize(context, Long.valueOf(usageChange));

            TextView usageText = (TextView) findViewById(R.id.usageText);
            usageText.setText(formatredUsageChange);

        }
    };

    @Override
    protected void onPause() {
        //unregisterReceiver(receiverWifi);
        super.onPause();
    }

    @Override
    protected void onResume() {
        //registerReceiver(receiverWifi, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_init, menu);
        menu.add(0, 0, 0, "Refresh");
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
