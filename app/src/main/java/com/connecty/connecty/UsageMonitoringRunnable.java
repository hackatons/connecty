package com.connecty.connecty;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.TrafficStats;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class UsageMonitoringRunnable implements Runnable{


    LocalBroadcastManager localBroadcastManager;

    public UsageMonitoringRunnable(LocalBroadcastManager localBroadcastManager) {
        this.localBroadcastManager = localBroadcastManager;
    }

    @Override
    public void run() {
        Log.d("Connecty", " Scan Usage");
        scanUsage();
    }

    private void scanUsage() {

        String receivedUsage = String.valueOf(TrafficStats.getTotalRxBytes());
        Log.d("Connecty", "Broadcasting change in usage" + receivedUsage);
        Intent intent = new Intent("change-usage-event");
        intent.putExtra("usageChange", receivedUsage);
        localBroadcastManager.sendBroadcast(intent);
    }

}
